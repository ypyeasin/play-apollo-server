# Play Apollo Server

This is playgound project for learning Graph QL with Apollo

# How To run

Execute the following commands:

- `yarn` or `npm install`
- `yarn start` or `npm start` to start the project
- Goto [GraphiQL](http://localhost:4000/graphql)

Use following query to fetch the posts
`query getPosts{ getPosts{ userId id title body } }`

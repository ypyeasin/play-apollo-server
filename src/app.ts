import express from "express";
import graphqlHTTP from "express-graphql";
import { buildSchema } from "graphql";
import axios from "axios";

const schema = buildSchema(`
  type post {
    userId: Int
    id: Int
    title: String
    body: String
  }  
  type Query {
    hello: String
    getPosts: [post]
  }
`);

const root = {
  hello: () => "Hello World",
  getPosts: async () => {
    const result = await axios.get(
      "https://jsonplaceholder.typicode.com/posts"
    );
    return result.data;
  }
};

const app = express();
app.use(
  "/graphql",
  graphqlHTTP({
    schema: schema,
    rootValue: root,
    graphiql: true
  })
);

app.use("/test", async (req, res) => {
  const result = await axios.get("https://jsonplaceholder.typicode.com/posts");
  res.send(result.data);
});

app.listen(4000, () => console.log("Now browse to localhost:4000/graphql"));
